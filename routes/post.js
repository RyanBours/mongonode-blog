const config = require('../config.json');

const express = require('express');
const router = express.Router();

const MongoClient = require('mongodb').MongoClient;
const ObjectId = require('mongodb').ObjectId;
let reference = require('../reference');

function isAuthenticated(req, res, next) {
	if (req.user) return next();
	res.redirect('/login');
}

function findpost(req, res, next) {
	// TODO: Projection
	MongoClient.connect(reference.db_url, function(err, db) {
		if (err) throw err;
		if (req.params.id.length != 12) {
			db.collection("Posts").aggregate([
				{'$match': {_id:new ObjectId(req.params.id)} },
				{'$lookup': {
						from:"Users",
						localField:"userid",
						foreignField:"_id",
						as:"userid"
					}
				}
			]).toArray(function(err, result) {
				if (result != 0) {
					req.post = result;
					db.close();
					return next();
				}
				db.close();
				res.end("Can't find post or post got deleted.");
			});
		} else {
			db.close();
			res.end("Invalid post id.");
		}
	});
}

function findcomments(req, res, next) {
	// TODO: Projection
	MongoClient.connect(reference.db_url, function(err, db) {
		if (err) throw err;
		db.collection("Comments").aggregate([
			{ '$match': { postid:new ObjectId(req.params.id) } },
			{ '$unwind':"$comments" },
			{ '$lookup': {
					from:"Users",
					localField:"comments.userid",
					foreignField:"_id",
					as:"comments.userid"
				}
			}
		]).toArray(function(err, result) {
			req.comments = result;
			db.close();
			return next();
		});
	});
}

router.get('/post/:id', findpost,  function(req, res) {
	// res.setHeader('Content-Type', 'application/json');
	// res.send(req.comments);
	res.render('post', {
		isLoggedIn: req.user ? true : false,
		title:req.post[0].title,
		post:req.post[0]
	});
});

router.post('/post/:id', isAuthenticated, function(req, res) {
	let comment = {
		userid:req.user[0]._id,
		text: req.body.text,
		date:new Date(Date.now())
	}

	MongoClient.connect(reference.db_url, function(err, db) {
		db.collection("Comments").updateOne({postid:new ObjectId(req.params.id)}, {'$push':{"comments":comment}});
		db.close();
	});

	res.redirect('/post/'+req.params.id);
});

router.get('/comments/:id', findcomments, function(req, res) {
	res.render('comments', {
		comments:req.comments
	});
});

router.get('/create', isAuthenticated, function(req, res) {
	res.render('create', {
		isLoggedIn: req.user ? true : false,
		title:"create"
	});
});

router.post('/create', isAuthenticated, function(req, res) {
	let post = {
		title:req.body.title,
		text:req.body.text,
		userid:new ObjectId(req.user[0]._id),
		date:new Date(Date.now())
	}

	MongoClient.connect(reference.db_url, function(err, db) {
		if (err) throw err;
		db.collection("Posts").insertOne(post, function(err, result) {
			if (err) res.redirect('/create');

			let comment = {
				postid:result.ops[0]._id,
				comments:[]
			}

			db.collection("Comments").insertOne(comment, function(err, r) {
				db.close();
				res.redirect('/post/'+result.ops[0]._id);	
			});
			
		});
	});
});

router.get('/edit/:id', function(req, res) {
	res.end('edit');
});

module.exports = router;