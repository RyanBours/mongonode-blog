const config = require('../config.json');

const express = require('express');
const router = express.Router();

const MongoClient = require('mongodb').MongoClient;
const ObjectId = require('mongodb').ObjectId;
let reference = require('../reference');

router.get(['/', '/page/', '/page/:number'], function(req, res) {
	MongoClient.connect(reference.db_url, function(err, db) {
		if (err) throw err;
		let postPerPage = req.query.amount ? parseInt(req.query.amount) : 20;
		console.log(postPerPage);
		db.collection("Posts").aggregate([
			{'$sort':{date:-1}},
			{'$match': {}},
			{'$skip':req.params.number != undefined ? postPerPage*req.params.number : 0},
			{'$limit':postPerPage},
			{'$lookup': {
					from:"Users",
					localField:"userid",
					foreignField:"_id",
					as:"userid"
				}
			},
			{'$lookup': {
					from:"Comments",
					localField:"_id",
					foreignField:"postid",
					as:"comments"
				}
			}
		]).toArray(function(err, result) {
			if (err) throw err;
			db.close();
			res.render('index', {
  				isLoggedIn: req.user ? true : false,
  				title:"index",
  				posts:result,
  				currentpage: req.params.number ? req.params.number:0
  			});
		});
	});
});

module.exports = router;