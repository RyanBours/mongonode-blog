const config = require('../config.json');

const express = require('express');
const router = express.Router();

const MongoClient = require('mongodb').MongoClient;
const ObjectId = require('mongodb').ObjectId;
let reference = require('../reference');

function isAuthenticated(req, res, next) {
	if (req.user) return next();
	res.redirect('/login');
}

router.get('/profile', isAuthenticated, function(req, res) {
	res.redirect('/profile/'+req.user[0]._id);
});

router.get('/profile/:id', function(req, res) {
	MongoClient.connect(reference.db_url, function(err, db) {
		db.collection("Users").find({_id:new ObjectId(req.params.id)}, {limit:1}).toArray(function(err, result) {
			db.close();
			res.render('profile', {
				isLoggedIn: req.user ? true : false,
				profile:result[0]
			});
		});
	});
});

module.exports = router;