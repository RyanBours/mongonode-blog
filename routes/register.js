const config = require('../config.json');

const express = require('express');
const router = express.Router();

const passwordHash = require('password-hash');
const session = require('express-session');
const passport = require('passport');

const MongoClient = require('mongodb').MongoClient;
const ObjectId = require('mongodb').ObjectId;
let reference = require('../reference');

function isNotAuthenticated(req, res, next) {
	if (!req.user) return next();
	res.redirect('/');
}

router.get('/register', isNotAuthenticated, function(req, res) {
	res.render('register', {
		isLoggedIn: req.user ? true : false,
		'title':"registration"
	});
});

router.post('/register', isNotAuthenticated, function(req, res) {
	MongoClient.connect(reference.db_url, function(err, db) {
		if (err) throw err;

		let user = {
			username:req.body.username,
			password:passwordHash.generate(req.body.password),
			mail:req.body.mail,
			dateRegistration: new Date(Date.now()),
			role:0
		};

		db.collection("Users").find({'$or':[{username:user.username}, {mail:user.mail}]}).toArray(function(err, result) {
			if (err) throw err;
			if (result.length == 0) {
				db.collection("Users").insertOne(user, function(err, insres) {
					if (err) throw err;
					db.close();
					req.login(insres["ops"], function(err) {
						if (err) throw err;
						return res.redirect('/');
					});
				});
			} else {
				db.close();
				res.render('register', {failed:true})
			}
		});
	});
});

module.exports = router;