const config = require('../config.json');

const express = require('express');
const router = express.Router();

const MongoClient = require('mongodb').MongoClient;
const ObjectId = require('mongodb').ObjectId;

function isAuthenticated(req, res, next) {
	if (req.user) return next();
	res.redirect('/login');
}

function hasPrivilege(role) {
	return function(req, res, next) {
    	if (role !== req.user[0].role) res.redirect('/');
    	else next();
	}
}
const hasAdminPrivilege = hasPrivilege(2);

router.get('/admin', isAuthenticated, hasAdminPrivilege, function(req, res) {
	res.end("admin");
});

module.exports = router;