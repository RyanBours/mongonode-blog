const config = require('../config.json');

const express = require('express');
const router = express.Router();

const session = require('express-session');
const passport = require('passport');

const MongoClient = require('mongodb').MongoClient;
const ObjectId = require('mongodb').ObjectId;

router.get('/login', function(req, res) {
	res.render('login', {
		isLoggedIn: req.user ? true : false,
		'title':"login"
	});
});

router.post('/login', passport.authenticate('local', {
	successRedirect: '/',
	failureRedirect: '/login',
	session: true
}));

router.get('/logout', function(req, res) {
	req.session.destroy();
	req.logout();
	res.redirect('/');
});

module.exports = router;