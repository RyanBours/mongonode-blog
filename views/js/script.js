window.onscroll = function() {
	stickyScroll();
};

function stickyScroll() {
	let nav = document.getElementById("navi");
	let off = nav.offsetTop;
	if(window.pageYOffset > off)
		nav.classList.add("fixed-top");
	else
		nav.classList.remove("fixed-top");
}