const path = require('path');
const config = require('./config.json');

const MongoClient = require('mongodb').MongoClient;
const ObjectId = require('mongodb').ObjectId;
let reference = require('./reference');

const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const router = express.Router();

const passwordHash = require('password-hash');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

const session = require('express-session');
const MongoDBStore = require('connect-mongodb-session')(session);
const store = new MongoDBStore({
	uri:reference.db_url,
	collection:"Sessions"
});

const routeAdmin	= require('./routes/admin');
const routeIndex 	= require('./routes/index');
const routePost 	= require('./routes/post');
const routeLogin 	= require('./routes/login');
const routeRegister = require('./routes/register');
const routeProfile 	= require('./routes/profile');

passport.use(new LocalStrategy(function(username, password, done) {
	MongoClient.connect(reference.db_url, function(err, db) {
		if (err) return done(err);
		db.collection("Users").find({'$or':[{username:username}, {mail:username}]}).toArray(function(err, user) {
			if (user.length == 1) {
				if (passwordHash.verify(password, user[0].password)) {
					return done(null, user);
				} else {
					return done(null, false, { message: 'invalid password'});
				}
			} else {
				return done(null, false, { message: 'invalid username'});
			}
			db.close();
		});
	});
}));
passport.serializeUser(function(user, done) {
	done(null, user);
});
passport.deserializeUser(function(user, done) {
  done(null, user);
});

app.set('views', __dirname + '/views');
app.set('view engine', "pug");
app.use(bodyParser.urlencoded({ extended: true }));

app.use(session({
  secret: config.SESSION_SECRET,
  resave: false,
  saveUninitialized: false,
  rolling: true,
  cookie: {
  	expires: 1000*60*20
  },
  store: store
}));
app.use(passport.initialize());
app.use(passport.session());

app.use('/', routeAdmin);
app.use('/', routeIndex);
app.use('/', routePost);
app.use('/', routeLogin);
app.use('/', routeRegister);
app.use('/', routeProfile);

app.use(express.static(path.join(__dirname + 'views')));

app.listen(reference.port, function() {
	console.log('listening on port %s!', reference.port);
});