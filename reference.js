exports.port = process.env.PORT || 8080;
exports.db_url = process.env.DB_URL || "mongodb://localhost:27017/blog-test";